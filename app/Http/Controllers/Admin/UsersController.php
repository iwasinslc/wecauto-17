<?php
namespace App\Http\Controllers\Admin;

use App\Exports\Admin\RanksExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\RequestBonusUser;
use App\Http\Requests\RequestPenaltyUser;
use App\Models\Currency;
use App\Models\Deposit;
use App\Models\ExchangeOrder;
use App\Models\OrderPiece;
use App\Models\PageViews;
use App\Models\Transaction;
use App\Models\TransactionType;
use App\Models\User;
use App\Models\UserRank;
use App\Models\Wallet;
use App\Models\Withdraw;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Spatie\Permission\Models\Role;
use Webpatser\Countries\Countries;
use Yajra\Datatables\Datatables;
use Maatwebsite\Excel\Facades\Excel;

class UsersController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin/users/index');
    }


    /**
     * @return mixed
     */
    public function export_ranks()
    {
        return Excel::download(new RanksExport(), 'ranks.csv',  \Maatwebsite\Excel\Excel::CSV);

    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function dataTable()
    {
        $users = User::select(\DB::raw('users.*, parent.id as parent_id, parent.login as parent_login'))->whereRaw('users.login != users.email')->leftJoin('users as parent', 'users.partner_id', '=', 'parent.my_id')->with(['rank'])->orderBy('users.created_at', 'desc');

        return Datatables::of($users)->addColumn('show', function ($user) {
            return route('admin.users.show', ['user' => $user->id]);
        })
            ->addIndexColumn()
            ->addColumn('edit', function ($user) {
            return route('admin.users.edit', ['user' => $user->id]);
        })->addColumn('wec_wallet', function (User $user) {
//            return $user->getUserWallet('WEC')->address;
                return '';
        })->addColumn('licence_status', function (User $user) {
//            return $user->activeLicence()&&$user->licence!==null ? $user->licence->name : __('empty');
                return '';
        })->make(true);
    }


    /**
     * @param string $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function reset2fa($id)
    {
        $user = User::find($id);
        if ($user->setGoogle2FaDisabled()) {
            return  back()->with('success', __('2Fa has been deleted'));
        }
        return back()->with('error', __('Unable to delete 2fa'));
    }

    /**
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, User $user)
    {
        $level = $request->has('level') ? $request->level : 1;
        $plevel = $request->has('plevel') ? $request->plevel : 1;

        return view('admin/users/show', [
            'user' => $user,
            'level' => $level,
            'plevel' => $plevel,
        ]);
    }

    /**
     * @param $userId
     * @return mixed
     * @throws \Exception
     */
    public function dataTableWrs($userId)
    {
        /** @var Transaction $wrs */
        $wrs = Withdraw::select('withdraws.*')->with([
            'currency',
            'paymentSystem',
            'user',
            'wallet',
            'status'
        ])
            ->where('user_id', $userId);

        return Datatables::of($wrs)
            ->editColumn('status', function (Withdraw $transaction) {
                return $transaction->status->name;
            })
            ->editColumn('amount', function (Withdraw $transaction) {
                return number_format($transaction->amount, $transaction->wallet->currency->precision, '.', '');
            })
            ->make(true);
    }


    /**
     * @param $userId
     * @return mixed
     * @throws \Exception
     */
    public function dataTableTrades($userId)
    {
        $operations =  OrderPiece::where('user_id', $userId)->with(['user', 'currency', 'mainCurrency']);

        return DataTables::of($operations)
            ->make(true);
    }


    /**
     * @param $userId
     * @return mixed
     * @throws \Exception
     */
    public function dataTableOrders($userId)
    {
        $operations =  ExchangeOrder::where('user_id', $userId)->with(['user', 'currency', 'mainCurrency']);

        return DataTables::of($operations)
            ->make(true);
    }

    /**
     * @param $userId
     * @return mixed
     * @throws \Exception
     */
    public function dataTableDeposits($userId)
    {
        $deposits = Deposit::where('user_id', $userId)
            ->with('rate', 'currency')
            ->select('deposits.*');

        return Datatables::of($deposits)
            ->editColumn('condition', function ($deposit) {
                return __($deposit->condition);
            })
            ->make(true);
    }

    /**
     * @param $userId
     * @return mixed
     * @throws \Exception
     */
    public function dataTableTransactions($userId)
    {
        $transactions = Transaction::where('user_id', $userId)
            ->with('currency', 'type')
            ->select('transactions.*');

        return Datatables::of($transactions)
            ->addColumn('type_name', function ($transaction) {
                return __($transaction->type->name);
            })
            ->editColumn('approved', function ($transaction) {
                return __($transaction->approved == 1 ? 'yes' : 'no');
            })
            ->make(true);
    }

    /**
     * @param $userId
     * @return mixed
     * @throws \Exception
     */
    public function dataTablePageViews($userId)
    {
        $pageViews = PageViews::where('user_id', $userId)
            ->select('page_views.*');

        return Datatables::of($pageViews)
            ->addColumn('location', function ($pv) {
                $countries = Cache::rememberForever('countries', function () {
                    return Countries::select('iso_3166_2', 'flag')->get();
                });
                $flag = $countries->where('iso_3166_2', geoip($pv->user_ip)->iso_code)->first();
                return '<img src="' . secure_asset('flags') . '/' . $flag->flag . '"> ' . geoip($pv->user_ip)->country . ', ' . geoip($pv->user_ip)->city;
            })
            ->rawColumns(['location'])
            ->make(true);
    }

    /**
     * @param RequestBonusUser $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function bonus(RequestBonusUser $request)
    {
        $wallet = Wallet::find($request->wallet_id);
        if ($wallet->user_id != $request->user_id) {
            return back()->with('error', __('validation.wallet.wrong'));
        }
        if ($wallet) {
            $wallet = $wallet->addBonus($request->amount, $request->only(['source', 'batch_id']));
            // send notification to user
            $data = [
                'bonus_amount'   => $request->amount,
                'currency'       => $wallet->currency,
                'balance'        => $wallet->balance,
            ];
            $wallet->user->sendNotification('bonus_accrued', $data);
            return back()->with('success', __('Bonus accrued'));
        }
        return back()->with('error', __('Unable to accrue bonus'));
    }

    /**
     * @param RequestPenaltyUser $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function penalty(RequestPenaltyUser $request)
    {
        /** @var Wallet $wallet */
        $wallet = Wallet::find($request->wallet_id);

        if ($wallet) {
            $wallet->makePenalty($request->amount, 'Admin: ' . user()->name, true);
            // send notification to user
            $data = [
                'bonus_amount'   => $request->amount,
                'currency'       => $wallet->currency,
                'balance'        => $wallet->balance,
            ];
            $wallet->user->sendNotification('penalty_accrued', $data);
            return back()->with('success', __('Penalty handled'));
        }
        return back()->with('error', __('Unable to handle penalty'));
    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(User $user)
    {
        $roles = Role::all();
        $ranks = UserRank::query()->orderBy('id')->get();

        return view('admin/users/edit', ['roles' => $roles, 'user' => $user, 'ranks' => $ranks,]);
    }

    /**
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, User $user)
    {
        $this->validate($request, [
            'login' => 'bail|required|min:2',
            'email' => 'required|email',
            'password' => 'nullable|min:6',
            'sell_limit' => 'nullable|numeric',
            'buy_limit' => 'nullable|numeric',
            'deposit_sell_limit' => 'nullable|numeric',
        ]);






        if ($request->has('upline') && !empty($request->upline)) {
            $upline = trim($request->upline);
            $searchUpline = User::where('email', $upline)->orWhere('login', $upline)->orWhere('my_id', $upline)->first();

            if ($searchUpline !== null&&$user->partner_id != $searchUpline->my_id) {
                $user->partners()->detach();

                $user->partner_id = $searchUpline->my_id;
                $user->save();

                $user->generatePartnerTree($searchUpline);


                $referrals = $user->referrals()->get(); 
                foreach ($referrals as $referral) {
                    $referral->partners()->detach();
                    $referral->generatePartnerTree($referral->partner);
                }
            }
        }

        $representative = 1;

        if (!$request->has('representative')) {
            $representative = 0;
        }

        if ($user->update($request->except(['roles', 'password', 'upline', 'representative']))) {
            if (!empty($request->get('password'))) {
                $user->setPassword($request->password);
            }

            if (user()->hasRole(['root']))
            {
                if ($request->roles) {
                    $user->syncRoles($request->roles);
                }
                else
                {
                    $user->roles()->detach();
                }
            }


            $user->representative = $representative;
            $user->save();
            return redirect()->route('admin.users.show', ['user' => $user->id])->with('success', __('User has been updated'));
        } else {
            return back()->with('error', __('Unable to update user'))->withInput();
        }
    }

    /**
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        if ($user->delete()) {
            return redirect()->route('admin.users.index')->with('success', __('User has been deleted'));
        }
        return redirect()->route('admin.users.index')->with('error', __('Unable to delete user'));
    }
}
