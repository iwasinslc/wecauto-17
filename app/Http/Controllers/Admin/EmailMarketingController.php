<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RequestStartMailing;
use App\Models\User;

class EmailMarketingController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.email_marketing.index');
    }

    /**
     * @param RequestStartMailing $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function handle(RequestStartMailing $request)
    {
        $sendTo = $request->send_to;
        $title = $request->title;
        $content = $request->content;

        $i = 0;

        switch ($sendTo) {
            case "admins":
                User::notifyAdminsViaEmail('email_marketing', [
                    'content' => $content,
                    'title' => $title,
                ]);
                break;

            case "all":
                $users = User::all();

                foreach ($users as $user) {
                    $user->sendEmailNotification('email_marketing', [
                        'content' => $content,
                        'title' => $title,
                    ]);
                    $i++;
                }
                break;

            case "with_deposits":
                $users = User::whereHas('deposits', function($query) {
                    return $query->where('active', 1);
                });

                foreach ($users as $user) {
                    $user->sendEmailNotification('email_marketing', [
                        'content' => $content,
                        'title' => $title,
                    ]);
                    $i++;
                }
                break;

            case "without_deposits":
                $users = User::doesntHave('deposits');

                foreach ($users as $user) {
                    $user->sendEmailNotification('email_marketing', [
                        'content' => $content,
                        'title' => $title,
                    ]);
                    $i++;
                }
                break;
        }

        $i = $i == 0 ? 1 : 0;

        session()->flash('success', __('Created '.$i.' mailing jobs. They has been sent to work.'));

        return view('admin.email_marketing.index', [
            'title' => $title,
            'send_to' => $sendTo,
            'content' => $content,
        ]);
    }
}
