<?php
namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\RequestCharity;
use App\Http\Requests\RequestCreateDeposit;
use App\Models\Deposit;
use App\Models\Rate;
use App\Models\Transaction;
use App\Models\TransactionType;
use App\Models\User;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ConvertationController extends Controller
{

    public function index()
    {

        return view('profile.convertation');
    }

    /**
     * @param RequestCreateDeposit $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(RequestCharity $request)
    {



        if(!user()->activeLicence())
        {
            return back()->with('error', __('You do not have an active license.'));
        }

        $data = cache()->pull('protect-exchange-'.getUserId());

        if ($data!==null)
        {

            return back()->with('error', __('Error'));
        }

        cache()->put('protect-exchange-'.getUserId(), '1', 0.1);


        \DB::beginTransaction();
        try {

            $wallet = user()->wallets()->lockForUpdate()->find($request->wallet_id);

            if (!in_array($wallet->currency->code, ['ACC', 'GNT']))
            {
                return back()->with('error', __('Error'));
            }

            if ($wallet->balance - $wallet->balance * TransactionType::getByName('withdraw')->commission * 0.01 < $request->amount) {
                return back()->with('error', __('Requested amount exceeds the wallet balance'));
            }

            $fst_wallet = \user()->getUserWallet('FST');





            Transaction::convert($wallet, $request->amount);
            $wallet->update([
                'balance' => $wallet->balance - $request->amount
            ]);

            $fst_amount = $request->amount*\rate($wallet->currency->code, 'USD')*\rate('USD', 'FST');

            $fst_wallet->update([
                'balance' => $fst_wallet->balance + $fst_amount
            ]);

            \DB::commit();
        } catch(\Exception $e) {
            \DB::rollBack();
            return back()->with('error', $e->getMessage());
        }

        return back()->with('success', __('Convert Success'));
    }


}
