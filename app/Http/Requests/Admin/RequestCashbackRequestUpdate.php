<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class RequestCashbackRequestUpdate extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'approved' => 'sometimes|required|boolean',
            'documents_checked' => 'sometimes|required',
            'video_checked' => 'sometimes|required',
            'result' => 'sometimes|required|string',
        ];
    }
}
