<?php
return [
    'emails' => [
        'spam_protection'   => 'Surat ini tidak dikirim. Aturan spam.',
        'sent_successfully' => 'Surat berhasil dikirim',
        'unable_to_send'    => 'Tidak dapat mengirim surat ini',

        'request' => [
            'email_required' => 'Bidang email diperlukan',
            'email_max'      => 'Panjang maksimum email 255 karakter',
            'email_email'    => 'Bidang email tidak valid',

            'text_required' => 'Bidang text diperlukan',
            'text_min'      => 'Panjang teks minimum 10 karakter',
        ],
    ],
    'transaction_types' => [
        'enter'      => 'Isi ulang saldo',
        'withdraw'   => 'Penarikan dana',
        'bonus'      => 'Bonus',
        'partner'    => 'Komisi Afiliasi',
        'dividend'   => 'Penghasilan atas bunga deposito',
        'create_dep' => 'Membuka deposito',
        'close_dep'  => 'Menutup deposito',
        'penalty'    => 'Denda',
    ],

];
